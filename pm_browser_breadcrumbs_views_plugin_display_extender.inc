<?php
/**
 * @file
 * Definition of pm_browser_breadcrumbs_views_plugin_display_extender.
 */

/**
 * Adds the PM Browser breadcrumbs options to the display.
 *
 * @ingroup views_display_plugins
 *
 * @ignore style_class_names
 */
class pm_browser_breadcrumbs_views_plugin_display_extender extends views_plugin_display_extender {

  /**
   * Doing nothing.
   */
  function add_signature(&$view) {}
  /**
   * Provide a form to edit options for this plugin.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['pm_browser_breadcrumbs_active'] = array('default' => TRUE);
    return $options;
  }
  /**
   * Alter the options to allow for defaults.
   */
  function options_definition_alter(&$options) {
    $options['pm_browser_breadcrumbs_active'] = array('default' => TRUE);
  }
  /**
   * Provide a form to edit options for this plugin.
   */
  function options_form(&$form, &$form_state) {
    if (strpos($form['#section'], '-pm_browser') == TRUE) {
      $form['pm_browser_breadcrumbs_active'] = array(
        '#type' => 'checkbox',
        '#title' => t('Activate PM Browser breadcrumbs'),
        '#description' => t('Check to activate this breadcrumb override'),
        '#weight' => 1,
        '#default_value' => $this->display->get_option('pm_browser_breadcrumbs_active'),
      );
    }
  }

  /**
   * Handle any special handling on the validate form.
   */
  function options_submit(&$form, &$form_state) {
    if (isset($form_state['values']['pm_browser_breadcrumbs_active'])) {
      $this->display->set_option('pm_browser_breadcrumbs_active', $form_state['values']['pm_browser_breadcrumbs_active']);
    }
  }

  /**
   * Provide the default summary for options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    if ($this->view->base_table == 'node') {
      $active = check_plain(trim($this->display->get_option('pm_browser_breadcrumbs_active')));
      $options['pm_browser'] = array(
        'category' => 'other',
        'title' => t('PM Browser'),
        'value' => $active ? t('active') : t('inactive'),
        'desc' => t('PM Browser breadcrumbs settings.'),
      );
    }
  }
  /**
   * Inject anything into the query that the display_extender handler needs.
   */
  function query() {
    if ($this->view->base_table == 'node') {
      $options = $this->view->display_handler->options;
      if (isset($options['pm_browser_breadcrumbs_active'])) {
        $this->view->query->options['pm_browser_breadcrumbs_active'] = $options['pm_browser_breadcrumbs_active'];
      }
    }
  }

}
