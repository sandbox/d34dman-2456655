<?php

/**
 * @file
 * Provide an alternate browsing experience for Drupal PM.
 */

/**
 * Implements hook_views_data_alter().
 */
function pm_browser_views_data_alter(&$data) {
  $data['node']['pm_browser_handler_field_operation'] = array(
    'title' => t('PM Browser Operations'),
    'help' => t('Exposes contextual operations that could be performed on the given node.'),
    'field' => array(
      'handler' => 'pm_browser_handler_field_operation',
      'group' => t('Content'),
    ),
  );
}

/**
 * Implements hook_views_plugins().
 */
function pm_browser_views_plugins() {
  $path = drupal_get_path('module', 'pm_browser');
  $plugins = array();
  $plugins['display_extender']['pm_browser_breadcrumbs'] = array(
    'title' => t('PM Browser Breadcrumbs'),
    'help' => t('Drupal PM Browser breadcrumbs settings'),
    'path' => $path,
    'handler' => 'pm_browser_breadcrumbs_views_plugin_display_extender',
  );
  return $plugins;
}

/**
 * Implements hook_views_post_build().
 */
function pm_browser_views_post_build(&$view) {

  if ($view->base_table == 'node' AND !empty($view->query->options['pm_browser_breadcrumbs_active'])) {
    // @todo: make this configurable.
    $arg_id = 0;
    $bread = array('', l(t('Home'), '<front>'));
    $bread[] = l(t('Organizations'), 'pm/browser/pmorganization');
    if (isset($view->args[$arg_id])) {
      $node = node_load($view->args[$arg_id]);

      $parameters = array();
      $parents = _pm_browser_get_parent_details($node);
      foreach ($parents as $key => $value) {
        $parameters['query'][$key] = $value->nid;
      }

      switch ($node->type) {
        case 'pmissue':
          $bread[] = pm_browser_create_breadcrumb_link('pmorganization', $parents);
          $bread[] = pm_browser_create_breadcrumb_link('pmproject', $parents);
          if (!empty($parents['pmissue'])) {
            $bread[] = pm_browser_create_breadcrumb_link('pmissue', $parents);
          }
          break;

        case 'pmproject':
          $bread[] = pm_browser_create_breadcrumb_link('pmorganization', $parents);
          $bread[] = pm_browser_create_breadcrumb_link('pmproject', $parents);
          break;

        case 'pmorganization':
          $bread[] = pm_browser_create_breadcrumb_link('pmorganization', $parents);
          break;

      }
    }
    $view->build_info['breadcrumb'] = array();
    drupal_set_breadcrumb($bread);
  }
}

/**
 * Helper function to create breadcrumb links.
 */
function pm_browser_create_breadcrumb_link($type, $parents) {
  $title = '';
  $path  = NULL;
  $para  = array();
  if (!empty($parents[$type])) {
    $parent_node_title = $parents[$type]->title;
    $para['query'][$type] = $parents[$type]->nid;
    switch ($type) {
      case 'pmorganization':
        $title = t('Projects');
        $para['attributes']['title'] = t('All Projects under Organization: @name', array('@name' => $parent_node_title));
        $path = 'pm/browser/pmproject';
        break;

      case 'pmproject':
        $title = t('Issues');
        $para['attributes']['title'] = t('All Issues under Project: @name', array('@name' => $parent_node_title));
        $path = 'pm/browser/pmissue';
        break;

      case 'pmissue':
        $title = t('Sub Issues');
        $para['attributes']['title'] = t('All Sub Issue under Issue: @name', array('@name' => $parent_node_title));
        $path = 'pm/browser/subissues';
        break;

    }
  }

  // If path is set, return a link, otherwise just a title.
  return $path ? l($title, $path, $para) : $title;
}

/**
 * Get Node's parent details.
 */
function _pm_browser_get_parent_details($node) {
  $parents = array();
  _pm_browser_get_parent_detail($node, 'pmorganization', $parents);
  _pm_browser_get_parent_detail($node, 'pmproject', $parents);
  _pm_browser_get_parent_detail($node, 'pmissue', $parents);
  return $parents;
}

/**
 * Helper function to get single parent detail.
 */
function _pm_browser_get_parent_detail($node, $parent_type, &$parents) {
  $parent_nid = pmpermission_get_parent_nid_of_node($node, $parent_type);
  if ($parent_nid) {
    $parent_node = node_load($parent_nid);
    if ($parent_node) {
      $parents[$parent_type] = $parent_node;
    }
  }
}
