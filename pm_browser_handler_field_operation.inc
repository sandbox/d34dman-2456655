<?php
/**
 * @file
 * Field handler to present a link node edit and other contextual features.
 */

/**
 * Views handler to display contextual node links for Drupal PM nodes.
 */
class pm_browser_handler_field_operation extends views_handler_field_node_link {
  /**
   * Defines views field options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['display_add_icon'] = array('default' => TRUE);
    $options['display_icons'] = array('default' => TRUE);
    $options['display_theme'] = array('default' => 'ctools_dropdown');
    return $options;
  }

  /**
   * Defines views field option form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $options = array(
      'ctools_dropdown' => 'Ctools Dropdown',
      'links' => 'Links',
    );
    $form['display_theme'] = array(
      '#type' => 'select',
      '#title' => t('Theme'),
      '#description' => t('Theme function to use to render the menu links.'),
      '#default_value' => $this->options['display_theme'],
      '#options' => $options,
    );
    $options = array(
      'title' => t('Title liked to node'),
      'view' => t('View'),
      'edit' => t('Edit'),
      'delete' => t('Delete'),
      'add_pmorganization' => t('Add New Organization'),
      'add_pmproject' => t('Add New Project'),
      'add_pmissue' => t('Add New Issue'),
      'add_pmexpense' => t('Add New Expense'),
      'add_pmnote' => t('Add New Note'),
      'add_pmtimetracking' => t('Add New Timetracking'),
    );
    $form['links'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Theme'),
      '#description' => t('Links to be displayed if available.'),
      '#default_value' => $this->options['links'],
      '#options' => $options,
    );
  }

  /**
   * Renders field to show icon.
   */
  function render($values) {
    if ($node = $this->get_value($values)) {
      $id = $node->nid;
      $links = array();
      $title = $node->title;
      $path = "node/$id";
      if ($link = $this->create_link_definition($title, $path)) {
        $links['title'] = $link;
      }
      if ($link = $this->create_link_definition(t('view'), $path)) {
        $links['view'] = $link;
      }
      if ($link = $this->create_link_definition(t('edit'), "node/$id/edit")) {
        $links['edit'] = $link;
      }
      if ($link = $this->create_link_definition(t('delete'), "node/$id/delete")) {
        $links['delete'] = $link;
      }

      $title = t('Add New Organization');
      $path = "node/add/pmorganization";
      if ($link = $this->create_link_definition($title, $path)) {
        $links['add_pmorganization'] = $link;
      }

      $title = t('Add New Project');
      $path = "node/add/pmproject";
      if ($link = $this->create_link_definition($title, $path)) {
        $links['add_pmproject'] = $link;
      }

      $vars = array(
        'title' => t('Quick Links'),
        'links' => $links,
      );
      $theme = !empty($this->options['display_theme']) ? $this->options['display_theme'] : 'ctools_dropdown';
      $theme = 'ctools_dropdown';
      return theme($theme, $vars);
    }
  }

  /**
   * Check if path is valid and create links. returns false otherwise.
   */
  function create_link_definition($title, $path) {
    $link = FALSE;
    if (drupal_valid_path($path)) {
      $link = array(
        'title' => $title,
        'href' => $path,
      );
    }
    return $link;
  }

}
